# CMake generated Testfile for 
# Source directory: C:/workspace/third_party/bullet/bullet3/Extras/Serialize
# Build directory: C:/workspace/third_party/bullet/bullet3/build3/Extras/Serialize
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(BulletFileLoader)
subdirs(BulletXmlWorldImporter)
subdirs(BulletWorldImporter)
