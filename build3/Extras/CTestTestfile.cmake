# CMake generated Testfile for 
# Source directory: C:/workspace/third_party/bullet/bullet3/Extras
# Build directory: C:/workspace/third_party/bullet/bullet3/build3/Extras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(Serialize)
subdirs(ConvexDecomposition)
subdirs(HACD)
subdirs(GIMPACTUtils)
subdirs(InverseDynamics)
