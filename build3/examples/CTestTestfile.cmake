# CMake generated Testfile for 
# Source directory: C:/workspace/third_party/bullet/bullet3/examples
# Build directory: C:/workspace/third_party/bullet/bullet3/build3/examples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(HelloWorld)
subdirs(BasicDemo)
subdirs(ExampleBrowser)
subdirs(ThirdPartyLibs/Gwen)
subdirs(OpenGLWindow)
