

cmake_minimum_required(VERSION 2.8)

##assuming THIRD_PARTY_DIR

######################################
find_path( BULLET_SRC NAMES btBulletCollisionCommon.h PATHS ${THIRD_PARTY_DIR}/bullet/bullet3/src )
find_path( BULLET_LIBS_DEBUG NAMES BulletDynamics_Debug.lib PATHS ${THIRD_PARTY_DIR}/bullet/lib/Debug )
find_path( BULLET_LIBS_RELEASE NAMES BulletDynamics.lib PATHS ${THIRD_PARTY_DIR}/bullet/lib/Release )

function( link_bullet targetname )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet2FileLoader.lib          debug ${BULLET_LIBS_DEBUG}/Bullet2FileLoader_Debug.lib          )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet3Collision.lib           debug ${BULLET_LIBS_DEBUG}/Bullet3Collision_Debug.lib           )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet3Common.lib              debug ${BULLET_LIBS_DEBUG}/Bullet3Common_Debug.lib              )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet3Dynamics.lib            debug ${BULLET_LIBS_DEBUG}/Bullet3Dynamics_Debug.lib            )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet3Geometry.lib            debug ${BULLET_LIBS_DEBUG}/Bullet3Geometry_Debug.lib            )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/Bullet3OpenCL_clew.lib         debug ${BULLET_LIBS_DEBUG}/Bullet3OpenCL_clew_Debug.lib         )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletCollision.lib            debug ${BULLET_LIBS_DEBUG}/BulletCollision_Debug.lib            )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletDynamics.lib             debug ${BULLET_LIBS_DEBUG}/BulletDynamics_Debug.lib             )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletFileLoader.lib           debug ${BULLET_LIBS_DEBUG}/BulletFileLoader_Debug.lib           )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletInverseDynamics.lib      debug ${BULLET_LIBS_DEBUG}/BulletInverseDynamics_Debug.lib      )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletInverseDynamicsUtils.lib debug ${BULLET_LIBS_DEBUG}/BulletInverseDynamicsUtils_Debug.lib )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletSoftBody.lib             debug ${BULLET_LIBS_DEBUG}/BulletSoftBody_Debug.lib             )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletWorldImporter.lib        debug ${BULLET_LIBS_DEBUG}/BulletWorldImporter_Debug.lib        )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/BulletXmlWorldImporter.lib     debug ${BULLET_LIBS_DEBUG}/BulletXmlWorldImporter_Debug.lib     )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/ConvexDecomposition.lib        debug ${BULLET_LIBS_DEBUG}/ConvexDecomposition_Debug.lib        )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/GIMPACTUtils.lib               debug ${BULLET_LIBS_DEBUG}/GIMPACTUtils_Debug.lib               )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/gtest.lib                      debug ${BULLET_LIBS_DEBUG}/gtest_Debug.lib                      )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/gwen.lib                       debug ${BULLET_LIBS_DEBUG}/gwen_Debug.lib                       )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/HACD.lib                       debug ${BULLET_LIBS_DEBUG}/HACD_Debug.lib                       )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/LinearMath.lib                 debug ${BULLET_LIBS_DEBUG}/LinearMath_Debug.lib                 )
    target_link_libraries( ${targetname} optimized ${BULLET_LIBS_RELEASE}/OpenGLWindow.lib               debug ${BULLET_LIBS_DEBUG}/OpenGLWindow_Debug.lib               )
endfunction( link_boost_usual )


